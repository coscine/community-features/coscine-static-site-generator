###############################################################################
# Coscine Static Site Generator
# Copyright (c) 2023 <https://git.rwth-aachen.de/romin.benfer>
# Licensed under the terms of the MIT License.
###############################################################################

"""
Generates a static website for a Coscine Resource.
"""


from __future__ import annotations
from os import mkdir, listdir
from os.path import join, exists, isdir, isfile
import logging
from datetime import datetime
import shutil
from argparse import ArgumentParser, Namespace
from markdown2 import markdown
from jinja2 import Environment, FileSystemLoader
import coscine


logger = logging.getLogger("coscine-static-site-generator")
logging.basicConfig(level=logging.INFO)


def fetch_data_from_coscine(args: Namespace) -> dict:
    """
    Fetches the data for the website from Coscine using the users
    Coscine API token and the specified project and resource names.
    The data is returned as a python dict and also relayed to the
    jinja2 templates.
    """
    token: str = args.token
    project_name: str = args.project
    resource_name: str = args.resource
    logger.info("Fetching information from Coscine...")
    client = coscine.ApiClient(token, verbose=False)
    logger.info("Gathering information about Coscine API Token owner...")
    coscine_api_token_owner = client.self()
    logger.info(f"Acquiring resource handle for '{resource_name}'...")
    resource = client.project(project_name).resource(resource_name)
    logger.info("Assembling data for the generator...")
    return {
        "website_logo": args.logo,
        "website_favicon": args.favicon,
        "website_logo_link": "https://coscine.de",
        "project_name": project_name,
        "resource_name": resource_name,
        "resource_description": markdown(resource.description),
        "author_name": coscine_api_token_owner.display_name,
        "author_email": coscine_api_token_owner.email,
#        "author_organization": coscine_api_token_owner.organization,
        "resource_request_access_url": resource.access_url,
        "resource_license": resource.license,
        "resource_metadata_schema": resource.application_profile,
        "timestamp": datetime.now().strftime(r"%B %d %Y, %I:%M %p"),
        "files": [
            obj for obj in resource.files(recursive=True)
            if not obj.is_folder
        ],
        "resource_handle": resource
    }


def create_output_directory(output_path: str, assume_overwrite_ok: bool):
    """
    Creates the output directory for the static website. If a directory
    with the same name is already present at the specified location, it
    is either deleted or the user is prompted with a question on how
    to proceed.
    """
    if exists(output_path) and isdir(output_path):
        if not assume_overwrite_ok and input(
            "A directory with the same name is already present at the "
            "specified location for the output directory. "
            f"Delete directory '{output_path}' or abort? (y/n) : "
        ).upper() != "Y":
            raise FileExistsError(
                "Directory with the same name as the output directory "
                "already present at the specified location. Run again "
                "with --assume-yes to overwrite it or answer Y next time."
            )
        logger.info(f"Removing existing directory '{output_path}'...")
        shutil.rmtree(output_path)
    logger.info(f"Creating output directory '{output_path}'...")
    mkdir(output_path)


def gather_custom_files(custom_path: str) -> dict:
    """
    Gather all user supplied markdown files and process them for
    incorporating them into the final website.
    The markdown files are converted to html and returned as a dict
    with their filename as key and the content as value.
    """
    data = {}
    if custom_path is None:
        return data
    for filename in listdir(custom_path):
        filepath = join(custom_path, filename)
        if isfile(filepath) and filename.endswith(".md"):
            with open(filepath, "rt", encoding="utf-8") as file:
                data[filename.removesuffix(".md")] = markdown(file.read())
        else:
            logger.warning(f"Skipping custom file '{filename}'.")
    return data


def generate_custom_files(env: Environment, output: str, custom_data: dict):
    """
    Populates the output directory with the custom user supplied files.
    By calling this function after generate_regular_files() we can overwrite
    regular files like index.html or files.html with out own implementation.
    """
    for filename, content in custom_data.items():
        logger.info(f"Generating custom webpage {filename}.md...")
        template = env.get_template("partials/layout.html")
        filepath = join(output, f"{filename}.html")
        html = template.render(**data, content=content)
        with open(filepath, "wt", encoding="utf-8") as file:
            file.write(html)


def generate_regular_files(env: Environment, output: str, data: dict):
    """
    Populates the output directory with the regular base website files.
    """
    src_templates: str = join("src", "templates")
    for filename in listdir(src_templates):
        filepath = join(src_templates, filename)
        if isfile(filepath) and filename.endswith(".html"):
            logger.info(f"Generating webpage {filename}...")
            template = env.get_template(filename)
            html = template.render(**data)
            with open(join(output, filename), "wt", encoding="utf-8") as file:
                file.write(html)


def generate_downloadable_data(output: str, resource: coscine.Resource):
    logger.info("Generating downloadable data...")
    logger.info("Creating resource file index dataframe...")
    #dataframe = resource.dataframe()
    logger.info("Creating resource metadata rdf graph...")
    graph = resource.graph()
    logger.info("Writing file indices to download directory...")
    mkdir(join(output, "download"))
    #dataframe.to_csv(join(output, "download", "files.csv"))
    #dataframe.to_json(join(output, "download", "files.json"))
    #dataframe.to_excel(join(output, "download", "files.xlsx"))
    #dataframe.to_html(join(output, "download", "files.html"))
    logger.info("Writing metadata to download directory...")
    graph.serialize(join(output, "download", "metadata.ttl"), "turtle")
    graph.serialize(join(output, "download", "metadata.json"), "json-ld")


def populate_output_directory(output: str, data: dict):
    """
    Populates the output directory by copying the static web contents
    from the source directory and generating html files with the help
    of the templates and the data fetched from Coscine.
    """
    src_static: str = join("src", "static")
    src_templates: str = join("src", "templates")
    logger.info(f"Copying files from '{src_static}' to '{output}'...")
    shutil.copytree(src_static, join(output, "static"))
    if "custom_static" in data:
        shutil.copytree(data["custom_static"], join(output, "static"))
    generate_downloadable_data(output, data["resource_handle"])
    env = Environment(loader=FileSystemLoader(src_templates))
    generate_regular_files(env, output, data)
    generate_custom_files(env, output, data["custom_files"])


def parse_command_line_arguments():
    """
    Parses command line arguments specified at program startup
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        "-t",
        "--token",
        required=True,
        help="Set Coscine REST API Token"
    )
    parser.add_argument(
        "-p",
        "--project",
        required=True,
        help="Set Coscine Project Name"
    )
    parser.add_argument(
        "-r",
        "--resource",
        required=True,
        help="Set Coscine Resource Name"
    )
    parser.add_argument(
        "-o",
        "--output",
        default="public",
        help="Set output directory path including the directory name"
    )
    parser.add_argument(
        "-c",
        "--custom",
        help=(
            "Specify a custom directory for files which get added as "
            "additional pages on the resulting website if they are in "
            "markdown format."
        )
    )
    parser.add_argument(
        "-s",
        "--static",
        help=(
            "Specify additional files that get copied varbatim to "
            "the websites static directory. Useful for including custom "
            "images or logos which can then be referenced in "
            "custom markdown files or linked via other command line options "
            "such as the custom favicon option. You can also use this "
            "to override default static files such as the css stylesheet "
            "since the custom static directory is copied after the default "
            "static directory, files with the same filename are overwritten."
        )
    )
    parser.add_argument(
        "-l",
        "--logo",
        default="static/coscine_logo.gif",
        help="Specify path to an image file used as a custom website logo"
    )
    parser.add_argument(
        "-f",
        "--favicon",
        default="static/favicon.ico",
        help="Specify path to an icon used as a custom website favicon"
    )
    parser.add_argument(
        "-y",
        "--assume-yes",
        action="store_true",
        help="Automatically answer yes to all questions on the command line"
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_command_line_arguments()
    data = fetch_data_from_coscine(args)
    create_output_directory(args.output, args.assume_yes)
    data["custom_files"] = gather_custom_files(args.custom)
    if args.static:
        data["custom_static"] = args.static 
    populate_output_directory(args.output, data)
    logger.info(f"Generated static website under '{args.output}'.")
