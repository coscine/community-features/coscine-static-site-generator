# Coscine Static Site Generator
## About
[Coscine] provides a platform for research data management. Users can store
their research data in Coscine resources, tag them with metadata
and collaboratively work within a Coscine project.
Access to the data however is restricted to members of the respective
Coscine project. To enable data sharing with peers outside of Coscine
I have created this static site generator. Given a Coscine resource, it
generates a website which provides an overview over the research project
and data. Visitors of this website can then view and download the files.
Metadata is also accessible and even searchable with the help of
a SPARQL interface.  
The resource description in Coscine is interpreted by the static site
generator as markdown and rendered on the front page of the resulting
website. Additionally, users may extend their website with custom markdown
or html files by setting a command line argument, prompting the generator
to include user generated files in the final result.  
Best of all, since only static sites are generated, no penny has to be spent
on a server for webhosting, as there are plenty of free services for static
site hosting like GitLab and GitHub. 🥳

> **DISCLAIMER**  
> Please note that this python module is developed and maintained
> by individual members of the scientific community! It is not an official
> service that *RWTH Aachen University* provides support for.
> Please direct bug reports, feature requests and general
> questions at this repository via the issues feature.  
> RWTH ServiceDesk doesn't have a clue about what's happening here and
> most certainly will not be able to help you!

## Set up your own static site in 5 minutes 😎
*For detailed instructions have a look at the [example repository](https://git.rwth-aachen.de/coscine/community-features/coscine-custom-resource-frontend)!*  

It's really simple! All you need to do is:  
1. Create a public repository with [GitHub] or [GitLab] (free)
2. [Create a Coscine API Token]
3. Specify the API Token as a secret variable in the repository settings
4. Create a CI script for the respective hosting service
5. Create a CI pipeline schedule

Done! Your static site now automatically generates itself and refreshes its
contents every X hours (as specified in the CI schedule).  
You can add images in your repository and reference them in your resource
description markdown to make them show up on the front page.  

Of course you can also generate your site locally and test it on your
computer first, before deploying it to a server or webhosting platform.
Generate the site with python:  
```bash
cd ./coscine-static-site-generator
py -m src -t YOUR_COSCINE_API_TOKEN -p YOUR_PROJECT_NAME -r YOUR_RESOURCE_NAME
```
The script creates a directory `public/` which contains the final website.  
To test the resulting website locally, you can use the builtin python
http server:  
```bash
cd ./coscine-static-site-generator
py -m http.server 5001 --directory public/
```
Then navigate to `http://localhost:5001` in your web browser of choice.

## The technical details 🤓
Coscine provides temporary URLs for each file object stored inside one
of its resources. These URLs provide direct access to a file for
download, delete and update actions. No access token or other form
of authentication is required, since authentication is already
built into the URLs itself. To mitigate security risks, these URLs
are only valid for a certain amount of time.  
We can make use of these URLs and provide external users with access to our
research data by (re-)generating a static website in regular intervals.  
To fetch the URLs of the files, their metadata and various other kinds
of information from Coscine we use the [Coscine Python SDK]. We then feed
the results into a jinja2 website template and then generate the final
result with the jinja2 python module. With the website we provide a couple
of helpful javascript libraries, for more advanced user interfaces.  
The final result is a fully featured static website, which one can then
host on a webhosting platform and share with peers via a link for easy access
to their research data.

## License
This project is Open Source Software and licensed under
the terms of the [MIT License].

[Coscine]: https://coscine.de/
[Coscine Python SDK]: https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk/
[GitHub]: https://github.com/
[GitLab]: https://gitlab.com/
[Create a Coscine API Token]: https://docs.coscine.de/en/user/token/
[MIT License]: ./LICENSE.txt
